<?php require_once('../../../private/initialize.php'); ?>

<?php $id = $_GET['id'] ?? '1'; ?>
<?php $page_title = h($_GET['page'] ?? 'Page'); ?>

<?php include(SHARED_PATH . '/staff_header.php'); ?>


<div id="content">
  <a href="<?php echo url_for('/staff/subjects/index.php') ?>">&lt;&lt; Back to List</a>
  <p><?php echo 'Page ID: ' . h($id); ?></p>
</div>

<?php include(SHARED_PATH . '/staff_footer.php'); ?>
